import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'app/bindings/home_binding.dart';
import 'app/routes/app_pages.dart';
import 'app/routes/app_routes.dart';
import 'app/ui/android/home/home_pages.dart';
import 'app/ui/theme/app_colors.dart';

void main() {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    initialBinding: HomeBinding(),
    initialRoute: Routes.INITIAL,
    theme: ThemeData(
        primaryColor: UIColors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity),
    getPages: AppPages.pages,
    home: HomePages(),
  ));
}
