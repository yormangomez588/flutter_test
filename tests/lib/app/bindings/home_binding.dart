import 'package:get/get.dart';
import 'package:tests/app/controller/home/home_controller.dart';
import 'package:tests/app/data/provider/user_api.dart';
import 'package:tests/app/data/repository/user_repository.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.put<UserAPI>(UserAPI());
    Get.put<UserRepository>(UserRepository());
  }
}
