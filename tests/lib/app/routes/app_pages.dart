import 'package:get/get.dart';
import 'package:tests/app/bindings/home_binding.dart';
import 'package:tests/app/ui/android/home/home_pages.dart';

import 'app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: Routes.INITIAL,
      page: () => HomePages(),
      binding: HomeBinding(),
    ),
  ];
}
