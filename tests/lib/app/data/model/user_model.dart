class User {
  final String? id;
  final String? primerapellido,
      segundoapellido,
      primernombre,
      otronombre,
      pais,
      tipoidentidad,
      numeroindentidad,
      email,
      area;
  User({
    required this.id,
    required this.email,
    required this.primerapellido,
    required this.segundoapellido,
    required this.primernombre,
    required this.otronombre,
    required this.pais,
    required this.tipoidentidad,
    required this.numeroindentidad,
    required this.area,
  });

  static User fromJson(Map<String, dynamic> json) {
    return User(
      id: json['_id'],
      email: json['email'],
      primerapellido: json['primerapellido'],
      segundoapellido: json['segundoapellido'],
      primernombre: json['primernombre'],
      otronombre: json['otronombre'],
      pais: json['pais'],
      tipoidentidad: json['tipoidentidad'],
      numeroindentidad: json['numeroindentidad'],
      area: json['area'],
    );
  }
}
