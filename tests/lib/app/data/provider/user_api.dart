import 'dart:io';
import 'package:get/get.dart';
import 'package:tests/app/data/model/user_model.dart';

class UserAPI extends GetConnect {
  Future<List<User>> getAll() async {
    try {
      final response = await get(
        'http://10.0.2.2:3000/api/user',
        contentType: "application/json; charset=utf-8",
      );

      if (response.statusCode == 200) {
        final user =
            (response.body as List).map((e) => User.fromJson(e)).toList();

        return user;
      } else {
        return Future.error('Ocurrio un error al obtener la respuesta');
      }
    } on SocketException {
      return Future.error(
          'Se perdió la conexión con el servidor, por favor verifique su conexión a internet');
    } on Exception {
      return Future.error(
          'Ocurrio un error al obtener la respuesta del servidor');
    }
  }

  Future<void> postUser(
    String tipoIdentidad,
    String numeroIdentidad,
    String primerApellido,
    String segundoApellido,
    String primerNombre,
    String otroNombre,
    String paisEmpleado,
    String areaEmpelado,
  ) async {
    try {
      final data = {
        "primerapellido": primerApellido,
        "segundoapellido": segundoApellido,
        "primernombre": primerNombre,
        "otronombre": otroNombre,
        "pais": paisEmpleado,
        "tipoidentidad": tipoIdentidad,
        "numeroindentidad": numeroIdentidad,
        "area": areaEmpelado,
        "email": "$primerNombre$primerApellido@cidenet.com.$paisEmpleado"
            .toUpperCase()
      };

      final response = await post(
        'http://10.0.2.2:3000/api/user',
        data,
      );
      print(response.body);
    } catch (e) {
      print(e);
    }
  }

  Future<void> patchUser(
    String id,
    String tipoIdentidad,
    String numeroIdentidad,
    String primerApellido,
    String segundoApellido,
    String primerNombre,
    String otroNombre,
    String paisEmpleado,
    String areaEmpelado,
  ) async {
    try {
      final data = {
        "primerapellido": primerApellido,
        "segundoapellido": segundoApellido,
        "primernombre": primerNombre,
        "otronombre": otroNombre,
        "pais": paisEmpleado,
        "tipoidentidad": tipoIdentidad,
        "numeroindentidad": numeroIdentidad,
        "area": areaEmpelado,
        "email": "$primerNombre$primerApellido@cidenet.com.$paisEmpleado"
      };
      final response = await patch(
        'http://10.0.2.2:3000/api/user/$id',
        data,
      );

      print(response.body);
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteUser(String id) async {
    final response = await delete(
      'http://10.0.2.2:3000/api/user/$id',
    );
    print(response.body);
  }
}
