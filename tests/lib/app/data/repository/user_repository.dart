import 'package:get/get.dart';
import 'package:tests/app/data/model/user_model.dart';
import 'package:tests/app/data/provider/user_api.dart';

class UserRepository extends GetxService {
  final UserAPI _userAPIProvider = Get.find<UserAPI>();

  Future<List<User>> getAll() async => await _userAPIProvider.getAll();

  Future<void> postUser(
    String tipoIdentidad,
    String numeroIdentidad,
    String primerApellido,
    String segundoApellido,
    String primerNombre,
    String otroNombre,
    String paisEmpleado,
    String areaEmpelado,
  ) async =>
      await _userAPIProvider.postUser(
        tipoIdentidad,
        numeroIdentidad,
        primerApellido,
        segundoApellido,
        primerNombre,
        otroNombre,
        paisEmpleado,
        areaEmpelado,
      );

  Future<void> patchUser(
    String id,
    String tipoIdentidad,
    String numeroIdentidad,
    String primerApellido,
    String segundoApellido,
    String primerNombre,
    String otroNombre,
    String paisEmpleado,
    String areaEmpelado,
  ) async =>
      await _userAPIProvider.patchUser(
        id,
        tipoIdentidad,
        numeroIdentidad,
        primerApellido,
        segundoApellido,
        primerNombre,
        otroNombre,
        paisEmpleado,
        areaEmpelado,
      );

  Future<void> deleteUser(String id) async => _userAPIProvider.deleteUser(id);
}
