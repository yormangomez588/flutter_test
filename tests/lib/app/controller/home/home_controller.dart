import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tests/app/data/model/user_model.dart';
import 'package:tests/app/data/repository/user_repository.dart';

class HomeController extends GetxController {
  final _userRepository = Get.find<UserRepository>();

  List<User> user = <User>[].obs;
  String userId = '';

  String? selected = 'Selecciona Tipo De Identidad'.toUpperCase();
  String? selectedPais = 'Selecciona Pais Del Empleado'.toUpperCase();
  String? selectedArea = 'Selecciona Area Del Empleado'.toUpperCase();

  List<String> dropdow = [
    'Cedula De Ciudadania'.toUpperCase(),
    'Cedula De Extranjeria'.toUpperCase(),
    'Pasaporte'.toUpperCase(),
    'Permiso Especial'.toUpperCase(),
  ].obs;

  List<String> dropdowPais = [
    'co'.toUpperCase(),
    'Us'.toUpperCase(),
  ].obs;

  List<String> dropdowArea = [
    'Administrador'.toUpperCase(),
    'Finaciera'.toUpperCase(),
    'Compras'.toUpperCase(),
    'Infraestructura'.toUpperCase(),
    'Talento Humano'.toUpperCase(),
    'Operacion'.toUpperCase(),
    'Servicios Varios'.toUpperCase()
  ].obs;

  TextEditingController tipoIdentidad = TextEditingController();
  TextEditingController numeroIdentidad = TextEditingController();
  TextEditingController primerApellido = TextEditingController();
  TextEditingController segundoApellido = TextEditingController();
  TextEditingController primerNombre = TextEditingController();
  TextEditingController otroNombre = TextEditingController();
  TextEditingController paisEmpleado = TextEditingController();
  TextEditingController areaEmpelado = TextEditingController();

  @override
  void onInit() async {
    super.onInit();
    loadPublication();
  }

  List<DropdownMenuItem<String>> getOpcionesDropdow() {
    List<DropdownMenuItem<String>> lista;
    lista = [];
    dropdow.forEach((element) {
      lista.add(DropdownMenuItem(
        child: Text(element),
        value: element,
      ));
    });
    return lista;
  }

  List<DropdownMenuItem<String>> getOpcionesDropdowPais() {
    List<DropdownMenuItem<String>> lista;
    lista = [];
    dropdowPais.forEach((element) {
      lista.add(DropdownMenuItem(
        child: Text(element),
        value: element,
      ));
    });
    return lista;
  }

  List<DropdownMenuItem<String>> getOpcionesDropdowArea() {
    List<DropdownMenuItem<String>> lista;
    lista = [];
    dropdowArea.forEach((element) {
      lista.add(DropdownMenuItem(
        child: Text(element),
        value: element,
      ));
    });
    return lista;
  }

  Future<void> loadPublication() async {
    final data = await _userRepository.getAll();
    this.user = data;
    update();
  }

  editUser(String id) {
    print(id);
    this.userId = id;
  }

  Future<void> sumbitPost() async {
    await _userRepository.postUser(
      tipoIdentidad.text.toUpperCase(),
      numeroIdentidad.text.toUpperCase(),
      primerApellido.text.toUpperCase(),
      segundoApellido.text.toUpperCase(),
      primerNombre.text.toUpperCase(),
      otroNombre.text.toUpperCase(),
      paisEmpleado.text.toUpperCase(),
      areaEmpelado.text.toUpperCase(),
    );
    await loadPublication();
    clearTextController();
  }

  Future<void> sumbitPath() async {
    print(userId);
    await _userRepository.patchUser(
      userId,
      tipoIdentidad.text.toUpperCase(),
      numeroIdentidad.text.toUpperCase(),
      primerApellido.text.toUpperCase(),
      segundoApellido.text.toUpperCase(),
      primerNombre.text.toUpperCase(),
      otroNombre.text.toUpperCase(),
      paisEmpleado.text.toUpperCase(),
      areaEmpelado.text.toUpperCase(),
    );
    await loadPublication();
    clearTextController();
  }

  Future<void> sumbitDeleted(String id) async {
    print(id);
    await _userRepository.deleteUser(id);
    await loadPublication();
    clearTextController();
  }

  void clearTextController() {
    tipoIdentidad.clear();
    numeroIdentidad.clear();
    primerApellido.clear();
    segundoApellido.clear();
    primerNombre.clear();
    otroNombre.clear();
    paisEmpleado.clear();
    areaEmpelado.clear();
    this.selected = 'Selecciona Tipo De Identidad'.toUpperCase();
    this.selectedPais = 'Selecciona Pais Del Empleado'.toUpperCase();
    this.selectedArea = 'Selecciona Area Del Empleado'.toUpperCase();
    update();
  }
}
