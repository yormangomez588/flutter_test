import 'package:flutter/material.dart';
import 'layouts_shared.dart';

/// Different spacings based on [UILayout] sizes.

class UISpacing {
  static const Widget spacingH4 = SizedBox(width: UILayout.xsmall);
  static const Widget spacingV4 = SizedBox(height: UILayout.xsmall);

  static const Widget spacingH8 = SizedBox(width: UILayout.small);
  static const Widget spacingV8 = SizedBox(height: UILayout.small);

  static const Widget spacingH16 = SizedBox(width: UILayout.medium);
  static const Widget spacingV16 = SizedBox(height: UILayout.medium);

  static const Widget spacingH24 = SizedBox(width: UILayout.large);
  static const Widget spacingV24 = SizedBox(height: UILayout.large);

  static const Widget spacingH32 = SizedBox(width: UILayout.xlarge);
  static const Widget spacingV32 = SizedBox(height: UILayout.xlarge);

  static const Widget spacingH40 = SizedBox(width: UILayout.xxlarge);
  static const Widget spacingV40 = SizedBox(height: UILayout.xxlarge);

  static const Widget spacingH64 = SizedBox(width: UILayout.xxxlarge);
  static const Widget spacingV64 = SizedBox(height: UILayout.xxxlarge);
}
