import 'package:flutter/material.dart';
import 'package:tests/app/ui/theme/app_colors.dart';

class UIButtons {
  static Widget medium({
    Widget? child,
    void Function()? onPressed,
  }) =>
      ElevatedButton(
        onPressed: onPressed,
        child: child,
        style: ElevatedButton.styleFrom(
            primary: UIColors.purple,
            elevation: 10,
            fixedSize: Size(280, 50),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0))),
      );

  static Widget deleted({
    Widget? child,
    void Function()? onPressed,
  }) =>
      ElevatedButton(
        onPressed: onPressed,
        child: child,
        style: ElevatedButton.styleFrom(
            primary: Colors.red,
            elevation: 10,
            fixedSize: Size(80, 10),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0))),
      );

  static Widget semiBold({
    required void Function() onPressed,
    required Widget child,
    Color? color,
  }) =>
      ElevatedButton(
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(4),
            shadowColor: MaterialStateProperty.all(color ?? UIColors.purple),
            fixedSize: MaterialStateProperty.all(
              Size(164, 42),
            ),
            backgroundColor:
                MaterialStateProperty.all(color ?? UIColors.purple),
          ),
          onPressed: onPressed,
          child: child);
}
