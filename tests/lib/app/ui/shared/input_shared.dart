import 'package:flutter/material.dart';
import 'package:tests/app/ui/shared/paddings_shared.dart';
import 'package:tests/app/ui/theme/app_colors.dart';

class UITextInput {
  static inputName({
    TextEditingController? controller,
    String? label,
    int? max,
    TextInputType? keyboardType,
    String? Function(String?)? validator,
  }) =>
      Container(
          alignment: Alignment.center,
          padding: UIPadding.paddingH16,
          margin: UIPadding.padding_16,
          height: 64,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 50,
                    color: UIColors.purple.withOpacity(0.2))
              ]),
          child: TextFormField(
            keyboardType: keyboardType,
            textCapitalization: TextCapitalization.characters,
            controller: controller,
            maxLength: max,
            decoration: InputDecoration(
              hintStyle: TextStyle(
                color: UIColors.purple.withOpacity(0.6),
              ),
              labelText: label,
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              border: OutlineInputBorder(),
              isDense: true,
            ),
            validator: validator,
          ));

  static DropdownButton(
          {List<DropdownMenuItem<String>>? items,
          Widget? hint,
          String? Function(String?)? validator,
          void Function(String?)? onChanged}) =>
      Container(
        alignment: Alignment.center,
        padding: UIPadding.paddingH16,
        margin: UIPadding.padding_16,
        height: 54,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 10),
                  blurRadius: 50,
                  color: UIColors.purple.withOpacity(0.2))
            ]),
        child: DropdownButtonFormField(
          hint: hint,
          items: items,
          onChanged: onChanged,
          validator: validator,
          decoration: InputDecoration(
            hintStyle: TextStyle(
              color: UIColors.purple.withOpacity(0.6),
            ),
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            border: OutlineInputBorder(),
            isDense: true,
          ),
        ),
      );
}
