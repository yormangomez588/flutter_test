import 'package:flutter/material.dart';

class UIColors {
  /// [Primary-Group]
  ///
  static const Color purple = Color(0xFFBB6BD9);

  /// [Secondary-Group]

  static const Color select = Color(0xFFF2F9FF);
  static const Color background = Color(0xFFEBEFF2);
  static const Color white = Color(0xFFFFFFFF);
  static const Color black = Color(0xFF3C3C3C);
}
