import 'package:flutter/material.dart';

AppBar buildAppBar({Widget? child}) {
  return AppBar(
    title: Center(child: child),
    elevation: 0,
  );
}
