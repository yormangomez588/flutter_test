import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tests/app/controller/home/home_controller.dart';
import 'package:tests/app/ui/android/widget/app_bar.dart';
import 'package:tests/app/ui/shared/button_shared.dart';
import 'package:tests/app/ui/shared/input_shared.dart';
import 'package:tests/app/ui/shared/spacings_shared.dart';
import 'package:tests/app/ui/theme/app_colors.dart';

class CreaterUser extends StatelessWidget {
  const CreaterUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) => Scaffold(
        backgroundColor: UIColors.background,
        appBar: buildAppBar(),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UITextInput.DropdownButton(
                    hint: Text(controller.selected.toString()),
                    items: controller.getOpcionesDropdow(),
                    onChanged: (newValue) {
                      controller.selected = newValue;
                      controller.tipoIdentidad.text =
                          controller.selected.toString();
                    }),
                UITextInput.inputName(
                    controller: controller.numeroIdentidad,
                    label: 'Numero De Identidad'.toUpperCase(),
                    max: 20,
                    keyboardType: TextInputType.number),
                UITextInput.inputName(
                    controller: controller.primerApellido,
                    label: 'Primer Apellido'.toUpperCase(),
                    max: 20,
                    keyboardType: TextInputType.streetAddress),
                UITextInput.inputName(
                    controller: controller.segundoApellido,
                    label: 'Segundo Apellido'.toUpperCase(),
                    max: 20,
                    keyboardType: TextInputType.streetAddress),
                UITextInput.inputName(
                    controller: controller.primerNombre,
                    label: 'Primer Nombre'.toUpperCase(),
                    max: 20,
                    keyboardType: TextInputType.streetAddress),
                UITextInput.inputName(
                    controller: controller.otroNombre,
                    label: 'Otro Nombre'.toUpperCase(),
                    max: 50,
                    keyboardType: TextInputType.streetAddress),
                UITextInput.DropdownButton(
                    hint: Text(controller.selectedPais.toString()),
                    items: controller.getOpcionesDropdowPais(),
                    onChanged: (newValue) {
                      controller.selectedPais = newValue;
                      controller.paisEmpleado.text =
                          controller.selectedPais.toString();
                    }),
                UITextInput.DropdownButton(
                    hint: Text(controller.selectedArea.toString()),
                    items: controller.getOpcionesDropdowArea(),
                    onChanged: (newValue) {
                      controller.selectedArea = newValue;
                      controller.areaEmpelado.text =
                          controller.selectedArea.toString();
                    }),
                UISpacing.spacingV16,
                Center(
                    child: UIButtons.medium(
                        onPressed: () {
                          controller.sumbitPost();
                          Get.back();
                        },
                        child: Text("Registrar Empleado"))),
                UISpacing.spacingV16,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
