import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tests/app/controller/home/home_controller.dart';
import 'package:tests/app/data/model/user_model.dart';
import 'package:tests/app/ui/android/home/widget/creater_user.dart';
import 'package:tests/app/ui/android/home/widget/edit_user.dart';
import 'package:tests/app/ui/android/widget/app_bar.dart';
import 'package:tests/app/ui/shared/paddings_shared.dart';
import 'package:tests/app/ui/shared/spacings_shared.dart';
import 'package:tests/app/ui/theme/app_colors.dart';

class HomePages extends StatelessWidget {
  const HomePages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (controller) => Scaffold(
        backgroundColor: UIColors.background,
        appBar: buildAppBar(child: Text("home")),
        body: ListView.builder(
          itemCount: controller.user.length,
          itemBuilder: (context, index) {
            final User user = controller.user[index];
            return Card(
                child: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    UISpacing.spacingV8,
                    Padding(
                      padding: UIPadding.padding_8,
                      child: Row(
                        children: [
                          Text(user.primernombre.toString()),
                          UISpacing.spacingH8,
                          Text(user.otronombre.toString()),
                          UISpacing.spacingH8,
                          Text(user.primerapellido.toString()),
                          UISpacing.spacingH8,
                          Text(user.segundoapellido.toString())
                        ],
                      ),
                    ),
                    Padding(
                      padding: UIPadding.padding_8,
                      child: Row(
                        children: [
                          Text(user.tipoidentidad.toString()),
                          UISpacing.spacingH8,
                          Text(user.numeroindentidad.toString())
                        ],
                      ),
                    ),
                    Padding(
                      padding: UIPadding.padding_8,
                      child: Row(
                        children: [
                          Text(user.pais.toString()),
                          UISpacing.spacingH8,
                          Text(user.area.toString()),
                        ],
                      ),
                    ),
                    Padding(
                      padding: UIPadding.padding_8,
                      child: Row(
                        children: [
                          Text(user.email.toString()),
                        ],
                      ),
                    ),
                    UISpacing.spacingV8,
                  ],
                ),
                UISpacing.spacingH16,
                IconButton(
                    onPressed: () {
                      controller.editUser(user.id.toString());
                      Get.to(() => EditUser());
                    },
                    icon: Icon(Icons.edit, size: 35, color: UIColors.purple)),
                UISpacing.spacingH16,
                IconButton(
                    onPressed: () {
                      controller.sumbitDeleted(user.id.toString());
                    },
                    icon: Icon(Icons.delete, size: 35, color: UIColors.purple)),
                UISpacing.spacingH16,
              ],
            ));
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.to(() => CreaterUser());
          },
          child: Icon(Icons.add),
          backgroundColor: UIColors.purple,
        ),
      ),
    );
  }
}
